using System;
using UnityEngine;

namespace Tanx.ScenesManagement
{
    public interface IScenesLoader
    {
        void LoadScene(string sceneName);
        void UnLoadScene(string sceneName);
    }

    public interface IAsyncScenesLoader
    {
        public event Action<string> OnSceneReady;

        IProgressProvider LoadAsyncScene(string sceneName);
        void FinalizeLoad(string sceneName);

        IProgressProvider UnLoadAsyncScene(string sceneName);
    }
}
