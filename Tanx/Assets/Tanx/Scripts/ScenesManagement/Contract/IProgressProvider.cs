using QuasarBeam.Math;

namespace Tanx
{
    public interface IProgressProvider
    {
        public Percent Progress { get; }
    }
}
