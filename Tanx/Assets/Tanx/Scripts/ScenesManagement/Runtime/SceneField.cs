using UnityEngine;

namespace Tanx.ScenesManagement
{
    [System.Serializable]
    public class SceneField
    {
        [SerializeField]
        private Object m_SceneAsset;

        [SerializeField]
        private string m_SceneName = "";

        public string SceneName
        {
            get { return m_SceneName; }
        }

        public static implicit operator string(SceneField sceneField)
        {
            return sceneField.SceneName;
        }
    }
}
