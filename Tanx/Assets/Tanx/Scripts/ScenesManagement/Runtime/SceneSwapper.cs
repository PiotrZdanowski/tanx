using Tanx.UnityEvents;
using UnityEngine;
using QuasarBeam.Common;
using QuasarBeam.Math;
using UnityEngine.SceneManagement;

namespace Tanx.ScenesManagement
{
    public class SceneSwapper : MonoBehaviour
    {
        [SerializeField] private SceneField loadingScene;
        [SerializeField] private SceneField sceneToLoad;
        [SerializeField] private PercentUnityEvent progressBroadcast;
        [SerializeField, As(typeof(IScenesLoader))] private Object scenesLoader;
        [SerializeField, As(typeof(IAsyncScenesLoader))] private Object asyncScenesLoader;
        [SerializeField] private bool loadOnStart = true;

        private IProgressProvider loadedSceneProgressProvider;
        private Percent previousProgress;

        public void Start()
        {
            if (!loadOnStart)
            {
                return;
            }
            StartLoad();
        }

        public void StartLoad()
        {
            scenesLoader.As<IScenesLoader>().LoadScene(loadingScene);
            loadedSceneProgressProvider = asyncScenesLoader.As<IAsyncScenesLoader>().LoadAsyncScene(sceneToLoad);
            asyncScenesLoader.As<IAsyncScenesLoader>().OnSceneReady += OnSceneReady;
            previousProgress = 0.Percent();
        }

        private void OnSceneReady(string obj)
        {
            if (obj != sceneToLoad)
            {
                return;
            }
            asyncScenesLoader.As<IAsyncScenesLoader>().OnSceneReady -= OnSceneReady;
            var currentScene = SceneManager.GetActiveScene().name;
            asyncScenesLoader.As<IAsyncScenesLoader>().FinalizeLoad(sceneToLoad);
            asyncScenesLoader.As<IAsyncScenesLoader>().UnLoadAsyncScene(currentScene);
            asyncScenesLoader.As<IAsyncScenesLoader>().UnLoadAsyncScene(loadingScene);
            loadedSceneProgressProvider = null;
        }

        private void Update()
        {
            if(loadedSceneProgressProvider == null)
            {
                return;
            }
            if(loadedSceneProgressProvider.Progress == previousProgress)
            {
                return;
            }
            progressBroadcast.Raise(loadedSceneProgressProvider.Progress, this);
            previousProgress = loadedSceneProgressProvider.Progress;
        }
    }
}
