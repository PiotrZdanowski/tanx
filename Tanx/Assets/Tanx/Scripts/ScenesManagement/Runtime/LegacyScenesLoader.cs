using QuasarBeam.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tanx.ScenesManagement
{
    [CreateAssetMenu(menuName = "SO Based Legacy Scenes Loader")]
    public class LegacyScenesLoader : ScriptableObject, IScenesLoader, IAsyncScenesLoader
    {
        private Dictionary<string, SceneLoadingTracker> loads = new Dictionary<string, SceneLoadingTracker>();

        public event Action<string> OnSceneReady;

        public void FinalizeLoad(string sceneName)
        {
            if (!loads.ContainsKey(sceneName))
            {
                throw new InvalidOperationException($"{sceneName} is not being loaded!");
            }
            loads[sceneName].operation.allowSceneActivation = true;
            loads.Remove(sceneName);
        }

        public IProgressProvider LoadAsyncScene(string sceneName)
        {
            if(loads.ContainsKey(sceneName))
            {
                return loads[sceneName];
            }
            var operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            operation.allowSceneActivation = false;
            operation.completed += OnOperationCompleted;
            var tracker = new SceneLoadingTracker(operation);
            loads.Add(sceneName, tracker);
            return tracker;
        }

        private void OnOperationCompleted(AsyncOperation obj)
        {
            var load = loads.First(kvp => kvp.Value.operation == obj);
            OnSceneReady(load.Key);
        }

        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }

        public IProgressProvider UnLoadAsyncScene(string sceneName)
        {
            SceneManager.UnloadSceneAsync(sceneName);
            return new EmptyProgressProvider();
        }

        public void UnLoadScene(string sceneName)
        {
            UnLoadAsyncScene(sceneName);
        }

        private class EmptyProgressProvider : IProgressProvider
        {
            public Percent Progress => 0.Percent();
        }

        private class SceneLoadingTracker : IProgressProvider
        {
            public readonly AsyncOperation operation;

            public Percent Progress => Percent.From01Value(operation.progress);

            public SceneLoadingTracker(AsyncOperation operation)
            {
                this.operation = operation ?? throw new ArgumentNullException(nameof(operation));
            }
        }
    }
}
