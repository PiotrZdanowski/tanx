using QuasarBeam.Math;
using UnityEngine;

namespace Tanx.UnityEvents
{
    [CreateAssetMenu(fileName = "Percent Broadcast", menuName = CreateMenuEndpoints.CREATE_EVENT_ENDPOINT + "With Percent")]
    public class PercentUnityEvent : GenericUnityEvent<Percent> { }
}
