using UnityEngine;

namespace Tanx.UnityEvents
{
    public class GenericUnityEvent<TData> : ScriptableObject
    {
        public delegate void RaisedEventHandler(TData data, object sender);

        public event RaisedEventHandler OnRaised;

        public void Raise(TData data, object sender)
        {
            OnRaised?.Invoke(data, sender);
        }
    }
}
