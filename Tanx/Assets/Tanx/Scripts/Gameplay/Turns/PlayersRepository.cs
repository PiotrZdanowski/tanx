using QuasarBeam.Common;
using QuasarBeam.DDD;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tanx.Gameplay
{
    [CreateAssetMenu(menuName = "SO Based Players Repository")]
    public class PlayersRepository : ScriptableObject, IReadRepository<int, ITurnablePlayer>, IRepository<int, ITanxPlayer>
    {
        [SerializeField] private List<RepositoryEntry> players;

        public IEnumerator<RepositoryEntry<int, ITanxPlayer>> GetEnumerator()
        {
            return players.Select(e => new RepositoryEntry<int, ITanxPlayer>(e.ID, e.player.As<ITanxPlayer>())).GetEnumerator();
        }

        public void Insert(int id, ITanxPlayer entity)
        {
            players.Add(new RepositoryEntry(id, entity.AsComponent));
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator<RepositoryEntry<int, ITurnablePlayer>> IEnumerable<RepositoryEntry<int, ITurnablePlayer>>.GetEnumerator()
        {
            return players.Select(e => new RepositoryEntry<int, ITurnablePlayer>(e.ID, e.player.As<ITanxPlayer>())).GetEnumerator();
        }

        public bool Remove(int id)
        {
            players.Remove(players.First(e => e.ID == id));
            return true;
        }

        public void Awake()
        {
            players.Clear();
        }

        #region not implemented

        RepositoryEntry<int, ITurnablePlayer> IReadRepository<int, ITurnablePlayer>.Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public RepositoryEntry<int, ITanxPlayer> Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public int GetID(ITanxPlayer entity)
        {
            throw new System.NotImplementedException();
        }

        public int GetID(ITurnablePlayer entity)
        {
            throw new System.NotImplementedException();
        }

        public bool Contains(ITanxPlayer entity)
        {
            throw new System.NotImplementedException();
        }

        public bool Contains(ITurnablePlayer entity)
        {
            throw new System.NotImplementedException();
        }

        public bool ContainsID(int id)
        {
            throw new System.NotImplementedException();
        }
        #endregion

        [Serializable]
        private class RepositoryEntry
        {
            public int ID;
            public UnityEngine.Object player;

            public RepositoryEntry(int iD, UnityEngine.Object player)
            {
                ID = iD;
                this.player = player ?? throw new ArgumentNullException(nameof(player));
            }
        }
    }
}
