using QuasarBeam.Common;
using QuasarBeam.DDD;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tanx.Gameplay
{
    public class Turns : MonoBehaviour
    {
        [SerializeField, As(typeof(IReadRepository<int, ITurnablePlayer>))] private UnityEngine.Object _players;

        private IReadRepository<int, ITurnablePlayer> players;
        private Queue<ITurnablePlayer> playersQueue = new Queue<ITurnablePlayer>();
        private ITurnablePlayer currentPlayer;

        public event Action<ITurnablePlayer> OnPlayerTurnChanged;

        private void Awake()
        {
            players = _players.As<IReadRepository<int, ITurnablePlayer>>();
        }

        public void StartGame()
        {
            foreach (var player in players)
            {
                playersQueue.Enqueue(player.Entity);
                player.Entity.StartGame();
            }
            StartNextTurn();
        }

        private void StartNextTurn()
        {
            currentPlayer = playersQueue.Dequeue();
            currentPlayer.OnTurnFinished += OnCurrentPlayerFinishedTurn;
            currentPlayer.StartTurn();
            OnPlayerTurnChanged?.Invoke(currentPlayer);
        }

        private void OnCurrentPlayerFinishedTurn(ITurnablePlayer obj)
        {
            currentPlayer.OnTurnFinished -= OnCurrentPlayerFinishedTurn;
            playersQueue.Enqueue(obj);
            obj.FinalizeTurn();
            StartNextTurn();
        }

        public void StopGame()
        {
            currentPlayer.OnTurnFinished -= OnCurrentPlayerFinishedTurn;
            currentPlayer.TerminateTurn();
            playersQueue.Clear();
            currentPlayer = null;
        }
    }
}
