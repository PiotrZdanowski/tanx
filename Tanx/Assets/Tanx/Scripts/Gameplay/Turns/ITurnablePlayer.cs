﻿using System;

namespace Tanx.Gameplay
{
    public interface ITurnablePlayer
    {
        event Action<ITurnablePlayer> OnTurnFinished;

        void StartTurn();
        void StartGame();
        void TerminateTurn();
        void FinalizeTurn();
    }
}