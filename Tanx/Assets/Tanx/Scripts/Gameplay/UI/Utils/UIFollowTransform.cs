﻿using UnityEngine;

namespace Tanx.UI
{
    internal class UIFollowTransform : MonoBehaviour
    {
        [SerializeField] private Transform follow;

        private Camera mCamera;

        public Transform Follow
        {
            get => follow;
            set => follow = value;
        }
        public Vector3? FollowPosition { get; set; }

        public void Update()
        {
            if(follow != null)
            {
                SnapToPosition(follow.position);
            }
            else if(FollowPosition.HasValue)
            {
                SnapToPosition(FollowPosition.Value);
            }
        }

        public void SnapToPosition(Vector3 position)
        {
            if (mCamera == null)
            {
                mCamera = Camera.main;
            }

            Vector3 targPos = position;
            Vector3 camForward = mCamera.transform.forward;
            Vector3 camPos = mCamera.transform.position + camForward;
            float distInFrontOfCamera = Vector3.Dot(targPos - camPos, camForward);
            if (distInFrontOfCamera < 0f)
            {
                targPos -= camForward * distInFrontOfCamera;
            }
            transform.position = RectTransformUtility.WorldToScreenPoint(mCamera, targPos);
        }

        public void SnapToScreenPosition(Vector2 position)
        {
            transform.position = position;
        }
    }
}
