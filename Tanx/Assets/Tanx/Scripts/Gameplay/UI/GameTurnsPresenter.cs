using QuasarBeam.Common;
using QuasarBeam.DDD;
using System.Linq;
using Tanx.Gameplay;
using Tanx.Gameplay.UI;
using Tanx.UI;
using UnityEngine;

namespace Tanx
{
    public class GameTurnsPresenter : MonoBehaviour
    {
        [SerializeField] private Turns turns;
        [SerializeField, As(typeof(IReadRepository<int, ITanxPlayer>))] private UnityEngine.Object _players;
        [SerializeField] private GameTurnsView view;
        [SerializeField] private UIFollowTransform currentTurnIndicator;

        private IReadRepository<int, ITanxPlayer> players;

        private void Awake()
        {
            turns.OnPlayerTurnChanged += OnPlayerTurnChanged;
            players = _players.As<IReadRepository<int, ITanxPlayer>>();
        }

        private void OnPlayerTurnChanged(ITurnablePlayer obj)
        {
            var currentPlayer = players.First(e => e.Entity == obj).Entity; //using IDs would be more efficient but hey it works for now
            currentTurnIndicator.Follow = currentPlayer.AsComponent.transform;
            view.Show(new GameTurnsView.Data(players.Select(p => new GameTurnsView.Data.Player(p.Entity.Name, p.Entity.Points, p.Entity == obj)).ToList()));
        }
    }
}
