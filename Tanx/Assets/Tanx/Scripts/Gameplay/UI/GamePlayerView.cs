﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Tanx.Gameplay.UI
{
    internal class GamePlayerView : UIBehaviour
    {
        [SerializeField] private TMP_Text playerName;
        [SerializeField] private TMP_Text playerPoints;
        [SerializeField] private Color activePlayerColor = Color.red;
        [SerializeField] private Color defaultPlayerColor = Color.black;

        internal void Show(GameTurnsView.Data.Player player)
        {
            playerName.text = player.name;
            playerPoints.text = player.points.ToString();
            playerName.color = player.hasTurn ? activePlayerColor : defaultPlayerColor;
        }
    }
}