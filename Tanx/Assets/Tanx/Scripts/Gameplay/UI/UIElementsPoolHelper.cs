using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tanx
{
    public class UIElementsPoolHelper<TViewScript> where TViewScript : Component
    {
        private TViewScript playerViewPrefab;
        private Transform viewsParent;
        private List<TViewScript> currentViews;

        public event Action<int, TViewScript> OnNewInstantiated;

        public UIElementsPoolHelper(TViewScript playerViewPrefab, Transform viewsParent, List<TViewScript> currentViews)
        {
            this.playerViewPrefab = playerViewPrefab ?? throw new ArgumentNullException(nameof(playerViewPrefab));
            this.viewsParent = viewsParent ?? throw new ArgumentNullException(nameof(viewsParent));
            this.currentViews = currentViews ?? throw new ArgumentNullException(nameof(currentViews));
        }

        public void Show(int count, Action<int, TViewScript> fillFunction)
        {
            if (currentViews.Count < count)
            {
                int needed = count - currentViews.Count;
                for (int i = 0; i < needed; i++)
                {
                    var newView = GameObject.Instantiate(playerViewPrefab, viewsParent);
                    OnNewInstantiated?.Invoke(currentViews.Count, newView);
                    currentViews.Add(newView);
                }
            }
            for (int i = 0; i < count; i++)
            {
                fillFunction?.Invoke(i, currentViews[i]);
            }
            int toRemove = currentViews.Count - count;
            for (int i = 0; i < toRemove; i++)
            {
                GameObject.Destroy(currentViews[^(i + 1)].gameObject);
            }
            currentViews.RemoveRange(currentViews.Count - toRemove, toRemove);
        }
    }
}
