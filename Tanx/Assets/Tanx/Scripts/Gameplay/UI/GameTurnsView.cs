using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Tanx.Gameplay.UI
{
    public class GameTurnsView : UIBehaviour
    {
        [SerializeField] private GamePlayerView playerViewPrefab;
        [SerializeField] private Transform viewsParent;
        [SerializeField] private List<GamePlayerView> currentViews;

        private UIElementsPoolHelper<GamePlayerView> poolHelper;

        protected override void Awake()
        {
            poolHelper = new UIElementsPoolHelper<GamePlayerView>(playerViewPrefab, viewsParent, currentViews);
        }

        public void Show(Data data)
        {
            poolHelper.Show(data.players.Count, (i, view) => view.Show(data.players[i]));
        }

        public struct Data
        {
            public readonly IList<Player> players;

            public Data(IList<Player> players)
            {
                this.players = players;
            }

            public struct Player
            {
                public readonly string name;
                public readonly int points;
                public readonly bool hasTurn;

                public Player(string name, int points, bool hasTurn)
                {
                    this.name = name ?? throw new ArgumentNullException(nameof(name));
                    this.points = points;
                    this.hasTurn = hasTurn;
                }
            }
        }
    }
}
