using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tanx.Gameplay.UI
{
    public class GameOverView : MonoBehaviour
    {
        [SerializeField] private GamePlayerView playerRowPrefab; // a shortcut, to be refactored
        [SerializeField] private GamePlayerView winnerRow;
        [SerializeField] private Transform rowsParent;
        [SerializeField] private List<GamePlayerView> currentViews;

        private UIElementsPoolHelper<GamePlayerView> poolHelper;

        public event Action OnReturnToMainMenuRequested;

        private void Awake()
        {
            poolHelper = new UIElementsPoolHelper<GamePlayerView>(playerRowPrefab, rowsParent, currentViews);
        }

        public void Show(Data data)
        {
            winnerRow.Show(new GameTurnsView.Data.Player(data.winner.name, data.winner.points, false));
            poolHelper.Show(data.otherPlayers.Count, (i, view) => view.Show(new GameTurnsView.Data.Player(data.otherPlayers[i].name, data.otherPlayers[i].points, false)));
        }

        public void RequestReturnToMainMenu()
        {
            OnReturnToMainMenuRequested?.Invoke();
        }

        public struct Data
        {
            public readonly Player winner;
            public readonly IList<Player> otherPlayers;

            public Data(Player winner, IList<Player> otherPlayers)
            {
                this.winner = winner;
                this.otherPlayers = otherPlayers ?? throw new ArgumentNullException(nameof(otherPlayers));
            }

            public struct Player
            {
                public readonly string name;
                public readonly int points;

                public Player(string name, int points)
                {
                    this.name = name ?? throw new ArgumentNullException(nameof(name));
                    this.points = points;
                }
            }
        }
    }
}
