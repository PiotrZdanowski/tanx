﻿using QuasarBeam.Common;
using QuasarBeam.DDD;
using System;
using System.Linq;
using UnityEngine;

namespace Tanx.Gameplay
{
    internal class PlayersSpawner : MonoBehaviour
    {
        [SerializeField, As(typeof(IFactory<PlayerData, ITanxPlayer>))] private UnityEngine.Object _playersFactory;
        [SerializeField] private PlayersConfig playersConfig;
        [SerializeField, As(typeof(IRepository<int, ITanxPlayer>))] private UnityEngine.Object _playersContainer;
        [SerializeField] private Transform[] spawnPoints;

        private IFactory<PlayerData, ITanxPlayer> playersFactory;
        private IRepository<int, ITanxPlayer> playersContainer;

        private void Awake()
        {
            playersFactory = _playersFactory.As<IFactory<PlayerData, ITanxPlayer>>();
            playersContainer = _playersContainer.As<IRepository<int, ITanxPlayer>>();
        }

        public void SpawnPlayers()
        {
            int i = 0;
            foreach(var player in playersConfig.Players)
            {
                var playerObject = playersFactory.Create(player);
                playerObject.AsComponent.transform.position = spawnPoints[i].position;
                playerObject.AsComponent.transform.rotation = spawnPoints[i].rotation;
                playersContainer.Insert(i, playerObject);
                i++;
            }
        }

        private void OnDestroy()
        {
            DestroyPlayers();
        }

        internal void DestroyPlayers()
        {
            foreach(var player in playersContainer.ToArray())//caching all players to new collection (avoid enumeration errors)
            {
                playersContainer.Remove(player);
                Destroy(player.Entity.AsComponent.gameObject);
            }
        }
    }
}
