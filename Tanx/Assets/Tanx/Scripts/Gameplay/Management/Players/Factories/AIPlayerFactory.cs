﻿using QuasarBeam.Common;
using QuasarBeam.DDD;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class AIPlayerFactory : MonoBehaviour, IFactory<ITanxPlayer>
    {
        [SerializeField] private AIPlayer prefab;
        [SerializeField] private Transform parent;

        public ITanxPlayer Create()
        {
            var obj = Instantiate(prefab, parent);
            return obj;
        }
    }
}
