﻿using QuasarBeam.DDD;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class HumanPlayerFactory : MonoBehaviour, IFactory<ITanxPlayer>
    {
        [SerializeField] private HumanPlayer prefab;
        [SerializeField] private Transform parent;

        public ITanxPlayer Create()
        {
            return Instantiate(prefab, parent);
        }
    }
}
