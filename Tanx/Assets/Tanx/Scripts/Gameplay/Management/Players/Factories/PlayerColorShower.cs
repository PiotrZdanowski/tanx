﻿using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class PlayerColorShower : MonoBehaviour
    {
        [SerializeField] private Renderer[] renderersToColor;

        public void SetColor(Color color)
        {
            foreach(Renderer renderer in renderersToColor)
            {
                var mat = renderer.sharedMaterial;
                renderer.sharedMaterial = new Material(mat);
                renderer.sharedMaterial.color = color;
            }
        }
    }
}