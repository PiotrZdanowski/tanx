﻿using QuasarBeam.Common;
using QuasarBeam.DDD;
using System;
using System.Linq;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class PlayersFactory : MonoBehaviour, IFactory<PlayerData, ITanxPlayer>
    {
        [SerializeField] private Subfactory[] subfactories;

        public ITanxPlayer Create(PlayerData arg)
        {
            var obj = subfactories.First(f => f.CreatedPlayerType == arg.type).Factory.Create();
            obj.AsComponent.GetComponent<PlayerColorShower>().SetColor(arg.color);
            obj.Name = arg.Name;
            return obj;
        }

        [Serializable]
        private class Subfactory
        {
            [SerializeField, As(typeof(IFactory<ITanxPlayer>))] private UnityEngine.Object _factory;
            [field: SerializeField] public PlayerData.Type CreatedPlayerType { get; private set; }

            public IFactory<ITanxPlayer> Factory => _factory.As<IFactory<ITanxPlayer>>();
        }
    }
}
