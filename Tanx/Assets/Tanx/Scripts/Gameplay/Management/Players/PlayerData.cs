﻿using System;
using UnityEngine;

namespace Tanx.Gameplay
{
    [Serializable]
    internal class PlayerData
    {
        [field: SerializeField] public string Name { get; set; }
        [field: SerializeField] public Type type { get; set; }
        [field: SerializeField] public Color color { get; set; }

        public enum Type
        {
            HUMAN,
            AI
        }

        public PlayerData(string name, Type type, Color color)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            this.type = type;
            this.color = color;
        }
    }
}
