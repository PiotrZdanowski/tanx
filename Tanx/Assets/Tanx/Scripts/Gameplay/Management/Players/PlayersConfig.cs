﻿using System.Collections.Generic;
using UnityEngine;

namespace Tanx.Gameplay
{
    [CreateAssetMenu(menuName = "Players Spawn Config")]
    internal class PlayersConfig : ScriptableObject
    {
        [SerializeField] private List<PlayerData> players;
        [field: SerializeField] public int MinimumPlayerCount { get; private set; }
        [field: SerializeField] public int MaximumPlayerCount { get; private set; }
        [field: SerializeField] public PlayerData PlayerAddPrototype { get; private set; }

        public IList<PlayerData> Players => players;
    }
}
