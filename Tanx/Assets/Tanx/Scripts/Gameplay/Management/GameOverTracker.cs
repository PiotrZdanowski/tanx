using QuasarBeam.Common;
using QuasarBeam.DDD;
using Tanx.Gameplay;
using UnityEngine;

namespace Tanx
{
    public class GameOverTracker : MonoBehaviour
    {
        public delegate void GameOverEventHandler(ITanxPlayer winner);

        [SerializeField, As(typeof(IReadRepository<int, ITanxPlayer>))] private UnityEngine.Object _players;
        [SerializeField] private int maxPoints;

        public event GameOverEventHandler OnGameOver;

        public void StartTracking()
        {
            foreach (var player in _players.As<IReadRepository<int, ITanxPlayer>>())
            {
                player.Entity.OnPointAdded += OnPointAdded;
            }
        }

        private void OnPointAdded(ITanxPlayer obj)
        {
            if(obj.Points >= maxPoints)
            {
                foreach (var player in _players.As<IReadRepository<int, ITanxPlayer>>())
                {
                    player.Entity.OnPointAdded -= OnPointAdded;
                }
                OnGameOver?.Invoke(obj);
            }
        }
    }
}
