using QuasarBeam.Common;
using QuasarBeam.DDD;
using System.Linq;
using Tanx.Gameplay;
using Tanx.Gameplay.UI;
using Tanx.ScenesManagement;
using UnityEngine;

namespace Tanx
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private GameOverTracker gameOverTracker;
        [SerializeField] private Turns turns;
        [SerializeField] private PlayersSpawner playersSpawner;
        [SerializeField, As(typeof(IReadRepository<int, ITanxPlayer>))] private UnityEngine.Object _players;
        [SerializeField] private GameOverView gameOverView;
        [SerializeField] private SceneSwapper toMainMenuSceneSwapper;

        private void Start()
        {
            gameOverTracker.OnGameOver += OnGameOver;
            playersSpawner.SpawnPlayers();
            gameOverTracker.StartTracking();
            turns.StartGame();
        }

        private void OnGameOver(ITanxPlayer winner)
        {
            gameOverView.gameObject.SetActive(true);
            gameOverView.OnReturnToMainMenuRequested += OnReturnToMainMenuRequested;
            var otherPlayers = _players.As<IReadRepository<int, ITanxPlayer>>()
                        .Where(e => e.Entity != winner)
                        .Select(e => new GameOverView.Data.Player(e.Entity.Name, e.Entity.Points))
                        .OrderBy(d => d.points)
                        .ToList();
            gameOverView.Show(
                new GameOverView.Data(
                    new GameOverView.Data.Player(winner.Name, winner.Points),
                    otherPlayers
                    ));

            turns.StopGame();
            playersSpawner.DestroyPlayers();
        }

        private void OnReturnToMainMenuRequested()
        {
            gameOverView.OnReturnToMainMenuRequested -= OnReturnToMainMenuRequested;
            toMainMenuSceneSwapper.StartLoad();
        }
    }
}
