﻿using Tanx.Gameplay.Players.AI;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class MoveState : IState
    {
        private AIPlayer.ChosenTargetContainer targetContainer;
        private readonly Movement2D movement;
        private float desiredDistance;

        public MoveState(AIPlayer.ChosenTargetContainer targetContainer, Movement2D movement, float desiredDistance)
        {
            this.targetContainer = targetContainer;
            this.movement = movement;
            this.desiredDistance = desiredDistance;
        }

        public bool Finished { get; internal set; }

        public void Start()
        {
            Finished = false;
        }

        public void Stop()
        {

        }

        public void Tick()
        {
            var vector = targetContainer.Target.AsComponent.transform.position - movement.transform.position;
            if (vector.magnitude <= desiredDistance)
            {
                if (movement.IsMoving)
                {
                    return;
                }
                Finished = true;
                return;
            }
            movement.Move(Mathf.Sign(vector.x));
        }
    }
}