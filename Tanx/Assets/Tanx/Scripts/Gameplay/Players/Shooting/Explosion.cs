﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Tanx.Gameplay.Players
{
    internal class Explosion : MonoBehaviour
    {
        [SerializeField] float explosionRadius;
        [SerializeField] float explosionForce;
        [SerializeField] LayerMask effectLayer;
        [SerializeField] AnimationCurve forceFalloff;
        [SerializeField] private UnityEvent OnExplode;

        internal void Explode()
        {
            var catched = Physics2D.CircleCastAll(transform.position, explosionRadius, Vector2.right, float.PositiveInfinity, effectLayer);
            foreach(var item in catched)
            {
                var otherRb = item.rigidbody;
                if(otherRb == null)
                {
                    continue;
                }
                Vector2 forceDirection = otherRb.worldCenterOfMass - new Vector2(transform.position.x, transform.position.y);
                float applyForce = explosionForce * forceFalloff.Evaluate(forceDirection.magnitude / explosionRadius);
                item.rigidbody.AddForce(forceDirection.normalized * applyForce);
            }
            OnExplode?.Invoke();
        }

        public struct SpawnParams
        {
            public readonly Vector3 position;
            public readonly Vector3 hitNormal;

            public SpawnParams(Vector3 position, Vector3 hitNormal)
            {
                this.position = position;
                this.hitNormal = hitNormal;
            }
        }
    }
}
