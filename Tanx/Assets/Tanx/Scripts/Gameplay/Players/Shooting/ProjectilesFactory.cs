﻿using QuasarBeam.DDD;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    [CreateAssetMenu(menuName = "SO Based Projectiles Factory")]
    internal class ProjectilesFactory : BaseSOFactory<Projectile.SpawnParams, Projectile>
    {
        protected override void OnRecreated(Projectile.SpawnParams arg, Projectile launchedProjectileObject)
        {
            launchedProjectileObject.transform.position = arg.spawnPoint;
            launchedProjectileObject.transform.rotation = arg.spawnRotation;
            launchedProjectileObject.gameObject.SetActive(true);
            launchedProjectileObject.Launch();
            launchedProjectileObject.OnHit += LaunchedProjectileObject_OnHit;
        }

        private void LaunchedProjectileObject_OnHit(Projectile arg1, GameObject arg2)
        {
            arg1.OnHit -= LaunchedProjectileObject_OnHit;
            arg1.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            arg1.gameObject.SetActive(false);
        }
    }
}
