﻿using QuasarBeam.Common;
using QuasarBeam.DDD;
using System;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class Weapon : MonoBehaviour
    {
        [SerializeField] private Transform barrel;
        [SerializeField] private Transform shootPoint;
        [SerializeField] private float aimAngleSpeed = 10;
        [SerializeField] private float maxAimAngle = 180;
        [SerializeField] private float minAimAngle = 0;
        [SerializeField, As(typeof(IFactory<Projectile.SpawnParams, Projectile>))] private UnityEngine.Object _projectilesFactory;

        private float? desiredAngleMovement;

        public event Action<GameObject> OnHit;

        public void ChangeAim(float direction)
        {
            desiredAngleMovement = direction;
        }

        internal void Shoot()
        {
            var projectile = _projectilesFactory.As<IFactory<Projectile.SpawnParams, Projectile>>().Create(new Projectile.SpawnParams(shootPoint.position, shootPoint.rotation));
            projectile.OnHit += OnProjectileHit;
        }

        private void OnProjectileHit(Projectile arg1, GameObject arg2)
        {
            arg1.OnHit -= OnProjectileHit;
            OnHit?.Invoke(arg2);
        }

        private void FixedUpdate()
        {
            if(desiredAngleMovement.HasValue)
            {
                float newRotation = barrel.transform.localEulerAngles.z + desiredAngleMovement.Value * aimAngleSpeed;
                newRotation = Mathf.Clamp(newRotation, minAimAngle, maxAimAngle);
                barrel.transform.localEulerAngles = new Vector3(0, 0, newRotation);
            }
            desiredAngleMovement = null;
        }
    }
}