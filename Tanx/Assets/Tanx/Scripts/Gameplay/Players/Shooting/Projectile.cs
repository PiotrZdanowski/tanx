using QuasarBeam.Common;
using QuasarBeam.DDD;
using System;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private float launchSpeed;
        [SerializeField, As(typeof(IFactory<Explosion.SpawnParams, Explosion>))] private UnityEngine.Object _hitExplosionFactory;
        [SerializeField, As(typeof(IFactory<Explosion.SpawnParams, Explosion>))] private UnityEngine.Object _launchExplosionFactory;

        public event Action<Projectile, GameObject> OnHit;

        public void Launch()
        {
            rb.velocity = transform.forward * launchSpeed;
            _launchExplosionFactory.As<IFactory<Explosion.SpawnParams, Explosion>>().Create(new Explosion.SpawnParams(transform.position, transform.forward));
        }

        public struct SpawnParams
        {
            public readonly Vector3 spawnPoint;
            public readonly Quaternion spawnRotation;

            public SpawnParams(Vector3 spawnPoint, Quaternion spawnRotation)
            {
                this.spawnPoint = spawnPoint;
                this.spawnRotation = spawnRotation;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject go = collision.attachedRigidbody != null ? collision.attachedRigidbody.gameObject : collision.gameObject;
            _hitExplosionFactory.As<IFactory<Explosion.SpawnParams, Explosion>>().Create(new Explosion.SpawnParams(transform.position, -rb.velocity));
            OnHit?.Invoke(this, go);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            GameObject go = collision.otherRigidbody != null ? collision.otherRigidbody.gameObject : collision.gameObject;
            _hitExplosionFactory.As<IFactory<Explosion.SpawnParams, Explosion>>().Create(new Explosion.SpawnParams(transform.position, -rb.velocity));
            OnHit?.Invoke(this, go);
        }
    }
}
