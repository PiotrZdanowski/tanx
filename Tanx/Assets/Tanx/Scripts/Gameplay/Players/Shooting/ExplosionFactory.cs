﻿using QuasarBeam.DDD;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    [CreateAssetMenu(menuName = "SO Based Explosions Factory")]
    internal class ExplosionFactory : BaseSOFactory<Explosion.SpawnParams, Explosion>
    {
        protected override void OnRecreated(Explosion.SpawnParams arg, Explosion created)
        {
            created.transform.position = arg.position;
            created.transform.LookAt(arg.position + arg.hitNormal);
            created.Explode();
        }
    }
}
