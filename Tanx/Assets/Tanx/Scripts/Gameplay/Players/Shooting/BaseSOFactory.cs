﻿using QuasarBeam.DDD;
using System;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class BaseSOFactory<TIN, TOUT> : ScriptableObject, IFactory<TIN, TOUT>
        where TOUT : Component
    {
        [SerializeField] private TOUT prefab;

        private TOUT instance;

        public TOUT Create(TIN arg)
        {
            if(instance == null)
            {
                instance = Instantiate(prefab);
                OnCreated(instance);
            }
            OnRecreated(arg, instance);
            return instance;
        }

        protected virtual void OnCreated(TOUT created)
        {

        }

        protected virtual void OnRecreated(TIN arg, TOUT created)
        {

        }
    }
}
