﻿using Tanx.Gameplay.Players.AI;
using QuasarBeam.DDD;
using System.Linq;

namespace Tanx.Gameplay.Players
{
    internal class ChooseTargetState : IState
    {
        private IReadRepository<int, ITanxPlayer> players;
        private ITanxPlayer thisPlayer;
        private AIPlayer.ChosenTargetContainer targetContainer;

        public ChooseTargetState(AIPlayer.ChosenTargetContainer targetContainer, IReadRepository<int, ITanxPlayer> players, ITanxPlayer thisPlayer)
        {
            this.targetContainer = targetContainer;
            this.players = players;
            this.thisPlayer = thisPlayer;
        }

        public void Start() 
        {
            var closestNotThis = players.Aggregate(players.First(),
                (best, current) =>
                {
                    if(best.Entity == thisPlayer)
                    {
                        return current;
                    }
                    if(current.Entity == thisPlayer)
                    {
                        return best;
                    }
                    var currentDistance = (current.Entity.AsComponent.transform.position - thisPlayer.AsComponent.transform.position).sqrMagnitude;
                    var bestDistance = (best.Entity.AsComponent.transform.position - thisPlayer.AsComponent.transform.position).sqrMagnitude;
                    if (currentDistance < bestDistance)
                    {
                        return current;
                    }
                    return best;
                });
            targetContainer.Target = closestNotThis.Entity;
        }

        public void Stop() { }

        public void Tick() { }
    }
}