﻿using Tanx.Gameplay.Players.AI;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class AimState : IState
    {
        private const float AIM_HEIGHT = 1f;

        private AIPlayer.ChosenTargetContainer targetContainer;
        private readonly Weapon weapon;
        private readonly Transform bulletSpawnTransform;
        private readonly float noChangeDetectionTime;
        private readonly float acceptedAngle;

        private Quaternion previousBulletSpawnRotation;
        private float currentNoChangeTime;
        private float previousAngleChange = 0;

        public AimState(AIPlayer.ChosenTargetContainer targetContainer, Weapon weapon, Transform bulletSpawnTransform, float noChangeDetectionTime, float acceptedAngle)
        {
            this.targetContainer = targetContainer;
            this.weapon = weapon;
            this.bulletSpawnTransform = bulletSpawnTransform;
            this.noChangeDetectionTime = noChangeDetectionTime;
            this.acceptedAngle = acceptedAngle;
        }

        public bool Finished { get; internal set; }

        public void Start()
        {
            previousBulletSpawnRotation = bulletSpawnTransform.rotation;
            currentNoChangeTime = 0;
            Finished = false;
            previousAngleChange = 0;
        }

        public void Stop()
        {

        }

        public void Tick()
        {
            if (Finished)
            {
                return;
            }
            var targetAim = targetContainer.Target.AsComponent.transform.position + Vector3.up * AIM_HEIGHT;
            var angle = Vector3.SignedAngle(bulletSpawnTransform.forward, targetAim - bulletSpawnTransform.position, Vector3.forward);
            if(Mathf.Abs(angle) < acceptedAngle)
            {
                Finished = true;
                return;
            }
            weapon.ChangeAim(Mathf.Sign(angle));
            if(previousAngleChange == 0)
            {
                previousAngleChange = angle;
            }
            if (Mathf.Sign(previousAngleChange) != Mathf.Sign(angle)) //avoid jittering at close distance
            {
                Finished = true;
                return;
            }
            if (Quaternion.Angle(previousBulletSpawnRotation, bulletSpawnTransform.rotation) < 1)
            {
                currentNoChangeTime += Time.deltaTime;
            }
            else
            {
                currentNoChangeTime = 0;
                previousBulletSpawnRotation = bulletSpawnTransform.rotation;
            }
            if(currentNoChangeTime > noChangeDetectionTime)
            {
                Finished = true;
                return;
            }
        }
    }
}