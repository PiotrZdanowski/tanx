using QuasarBeam.Common;
using System;

namespace Tanx.Gameplay
{
    public interface ITanxPlayer : ITurnablePlayer, IUnityComponent
    {
        event Action<ITanxPlayer> OnPointAdded;

        public string Name { get; set; }
        public int Points { get; }
    }
}
