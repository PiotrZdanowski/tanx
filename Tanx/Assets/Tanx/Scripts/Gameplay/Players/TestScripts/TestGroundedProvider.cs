﻿using QuasarBeam.Math;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class TestGroundedProvider : MonoBehaviour, IGroundedProvider
    {
        [field: SerializeField] public bool IsGrounded { get; private set; }
    }
}
