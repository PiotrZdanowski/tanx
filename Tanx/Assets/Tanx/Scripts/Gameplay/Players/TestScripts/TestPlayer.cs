using System;
using System.Collections;
using System.Collections.Generic;
using Tanx.Gameplay;
using UnityEngine;

namespace Tanx.Gameplay
{
    public class TestPlayer : MonoBehaviour, ITanxPlayer
    {
        [SerializeField] private KeyCode finishTurnKeyCode;
        [SerializeField] private KeyCode finishTurnWithPointKeyCode;
        [field: SerializeField] public string Name { get; set; }

        public int Points { get; private set; }
        public Component AsComponent => this;

        public event Action<ITurnablePlayer> OnTurnFinished;
        public event Action<ITanxPlayer> OnPointAdded;

        private void Awake()
        {
            this.enabled = false;
        }

        public void StartTurn()
        {
            this.enabled = true;
        }

        private void Update()
        {
            if (Input.GetKeyUp(finishTurnKeyCode))
            {
                this.enabled = false;
                OnTurnFinished?.Invoke(this);
                return;
            }
            if (Input.GetKeyUp(finishTurnWithPointKeyCode))
            {
                this.enabled = false;
                Points++;
                OnPointAdded?.Invoke(this);
                OnTurnFinished?.Invoke(this);
                return;
            }
        }

        public void TerminateTurn()
        {
            FinalizeTurn();
        }

        public override string ToString()
        {
            return $"{Name} ({gameObject.name})";
        }

        public void StartGame()
        {
            this.enabled = false;
        }

        public void FinalizeTurn()
        {
            this.enabled = false;
        }
    }
}
