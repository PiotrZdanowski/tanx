﻿using Tanx.Gameplay.Players.AI;

namespace Tanx.Gameplay.Players
{
    internal class ShotState : IState
    {
        private Weapon weapon;

        public ShotState(Weapon weapon)
        {
            this.weapon = weapon;
        }

        public void Start()
        {
            weapon.Shoot();
        }

        public void Stop()
        {

        }

        public void Tick()
        {

        }
    }
}