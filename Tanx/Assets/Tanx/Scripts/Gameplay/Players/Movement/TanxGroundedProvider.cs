﻿using System;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class TanxGroundedProvider : MonoBehaviour, IGroundedProvider
    {
        [SerializeField] private Ray[] rays = new Ray[0];
        [SerializeField] public LayerMask groundLayers;
        [SerializeField] public float acceptedSlopeAngle = 40;

        public bool IsGrounded { get; private set; } = false;

        private void FixedUpdate()
        {
            bool allDetecting = true;
            foreach(var ray in rays)
            {
                var hit = Physics2D.Raycast(transform.TransformPoint(ray.RayOrigin), transform.TransformDirection(Vector3.down), ray.RayLength, groundLayers);
                if(hit == default || Vector2.Angle(Vector2.up, hit.normal) > acceptedSlopeAngle)
                {
                    allDetecting = false;
#if UNITY_EDITOR
                    DrawRay(ray, Color.red);
#endif
                    continue;
                }
#if UNITY_EDITOR
                DrawRay(ray, Color.green);
#endif
            }
            IsGrounded = allDetecting;
        }

        private void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying)
            {
                foreach (Ray ray in rays)
                {
                    DrawRay(ray, Color.cyan, gizmo: true);
                }
            }
        }

        private void DrawRay(Ray ray, Color color, bool gizmo = false)
        {
            GetRayDebugPoints(ray, out var origin, out var endPoint);
            if (gizmo)
            {
                Gizmos.color = color;
                Gizmos.DrawLine(origin, endPoint);
                return;
            }
            Debug.DrawLine(origin, endPoint, color);
        }

        private void GetRayDebugPoints(Ray ray, out Vector3 origin, out Vector3 endPoint)
        {
            origin = transform.TransformPoint(ray.RayOrigin);
            endPoint = origin + transform.TransformDirection(Vector3.down) * ray.RayLength;
        }

        [Serializable]
        private struct Ray
        {
            [field: SerializeField] public float RayLength { get; private set; }
            [field: SerializeField] public Vector2 RayOrigin { get; private set; }
        }
    }
}
