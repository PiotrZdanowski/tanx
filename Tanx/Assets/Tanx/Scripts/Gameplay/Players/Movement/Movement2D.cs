using QuasarBeam.Common;
using System;
using UnityEngine;

namespace Tanx
{
    public class Movement2D : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private float movementSpeed;
        [SerializeField] private float turnTorqueValue;
        [SerializeField, As(typeof(IGroundedProvider))] private UnityEngine.Object _groundedProvider;
        [SerializeField] private float damp = 1;
        [SerializeField] private float timeToTorque = 1;

        private Vector2 previousVelocity;
        private Vector2 currentVelocity;
        private float? currentMovementRequest;
        private IGroundedProvider groundedProvider;
        private float nonGroundedTime = 0;

        public bool IsMoving => !groundedProvider.IsGrounded || currentMovementRequest.HasValue || previousVelocity.magnitude > 0.1;

        private void Awake()
        {
            groundedProvider = _groundedProvider.As<IGroundedProvider>();
        }

        public void Move(float movement)
        {
            currentMovementRequest = movement;
        }

        private void FixedUpdate()
        {
            currentVelocity = Vector2.zero;
            ApplyDamp();
            ApplyInput();
            if(groundedProvider.IsGrounded)
            {
                Debug.DrawLine(transform.position, transform.position + new Vector3(currentVelocity.x, currentVelocity.y, 0));
                rb.gravityScale = 0;
                rb.velocity = currentVelocity;
                nonGroundedTime = 0;
            }
            else
            {
                nonGroundedTime += Time.deltaTime;
                rb.gravityScale = 1;
            }
            currentMovementRequest = null;
            previousVelocity = currentVelocity;
        }

        private void ApplyInput()
        {
            if (!currentMovementRequest.HasValue)
            {
                return;
            }
            if (groundedProvider.IsGrounded)
            {
                var desiredMovement = transform.TransformVector(Vector3.right * currentMovementRequest.Value * movementSpeed);
                Debug.DrawLine(transform.position, transform.position + desiredMovement, Color.red);
                desiredMovement += Vector3.down; //applying a little gravity to avoid jumping
                Debug.DrawLine(transform.position, transform.position + desiredMovement, Color.yellow);
                currentVelocity = new Vector2(desiredMovement.x, desiredMovement.y);
                return;
            }
            if(nonGroundedTime < timeToTorque)
            {
                return;
            }
            rb.AddTorque(currentMovementRequest.Value * turnTorqueValue);
        }

        private void ApplyDamp()
        {
            currentVelocity = previousVelocity / damp;
        }
    }
}
