﻿using QuasarBeam.Math;

namespace Tanx
{
    public interface IGroundedProvider
    {
        bool IsGrounded { get; }
    }
}