﻿namespace Tanx.Gameplay.Players.AI
{
    internal interface ITransition
    {
        IState From { get; }
        IState To { get; }

        bool ShouldTransition { get; }
    }
}