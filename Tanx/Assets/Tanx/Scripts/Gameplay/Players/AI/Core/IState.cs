﻿namespace Tanx.Gameplay.Players.AI
{
    internal interface IState
    {
        void Start();
        void Stop();
        void Tick();
    }
}