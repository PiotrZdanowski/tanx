﻿using System.Collections.Generic;
using System.Linq;

namespace Tanx.Gameplay.Players.AI
{
    internal class StateMachine
    {
        private readonly ICollection<ITransition> transitions;
        private readonly IState startState;

        private IState currentState;
        private IEnumerable<ITransition> currentStateTransitions;

        public StateMachine(ICollection<ITransition> transitions, IState startState)
        {
            this.transitions = transitions ?? throw new System.ArgumentNullException(nameof(transitions));
            this.startState = startState ?? throw new System.ArgumentNullException(nameof(startState));
        }

        public StateMachine(IState startState, params ITransition[] transitions) : this(transitions, startState) { }

        public void Tick()
        {
            if(currentState == null)
            {
                MoveToState(startState);
                return;
            }
            currentState.Tick();
            foreach(ITransition transition in currentStateTransitions)
            {
                if (transition.ShouldTransition)
                {
                    MoveToState(transition.To);
                    return;
                }
            }
        }

        private void MoveToState(IState state)
        {
            currentState?.Stop();
            currentState = state;
            currentStateTransitions = transitions.Where(t => t.From.Equals(currentState)).ToArray();
            currentState.Start();
        }
    }
}
