﻿using System;

namespace Tanx.Gameplay.Players.AI
{
    internal class BoolFuncDelegateTransition : ITransition
    {
        private readonly Func<bool> func;

        public IState From {get; private set;}
        public IState To { get; private set; }
        public bool ShouldTransition => func();

        public BoolFuncDelegateTransition(IState from, IState to, Func<bool> func)
        {
            this.func = func ?? throw new ArgumentNullException(nameof(func));
            From = from ?? throw new ArgumentNullException(nameof(from));
            To = to ?? throw new ArgumentNullException(nameof(to));
        }
    }
}
