﻿using System;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    internal class PrimitivePlayerInputReader : MonoBehaviour, IPlayerInputReader
    {
        [SerializeField] private KeyCode right;
        [SerializeField] private KeyCode left;
        [SerializeField] private KeyCode shoot;

        public event Action<float> OnMovementRequested;
        public event Action OnAimRequested;
        public event Action OnShootRequested;

        private void Update()
        {
            float movement = 0;
            bool anyInput = false;
            if (Input.GetKey(right))
            {
                movement += 1;
                anyInput = true;
            }
            if (Input.GetKey(left))
            {
                movement -= 1;
                anyInput = true;
            }
            if (anyInput)
            {
                OnMovementRequested?.Invoke(movement);
            }
            if (Input.GetKeyDown(shoot))
            {
                OnAimRequested?.Invoke();
            }
            if (Input.GetKeyUp(shoot))
            {
                OnShootRequested?.Invoke();
            }
        }
    }
}
