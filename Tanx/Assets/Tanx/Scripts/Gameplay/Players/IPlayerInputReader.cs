﻿using System;

namespace Tanx.Gameplay.Players
{
    public interface IPlayerInputReader
    {
        event Action<float> OnMovementRequested;
        event Action OnAimRequested;
        event Action OnShootRequested;
    }
}