﻿using Tanx.Gameplay.Players.AI;

namespace Tanx.Gameplay.Players
{
    internal class IdleState : IState
    {
        private AIPlayer.ChosenTargetContainer targetContainer;

        public IdleState(AIPlayer.ChosenTargetContainer targetContainer)
        {
            this.targetContainer = targetContainer;
        }

        public void Start()
        {
            targetContainer.Target = null;
        }

        public void Stop() { }

        public void Tick() { }
    }
}