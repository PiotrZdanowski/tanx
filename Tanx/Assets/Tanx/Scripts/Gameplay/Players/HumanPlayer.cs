﻿using QuasarBeam.Common;
using System;
using UnityEngine;

namespace Tanx.Gameplay.Players
{
    [SelectionBase]
    internal class HumanPlayer : MonoBehaviour, ITanxPlayer
    {
        [SerializeField, As(typeof(IPlayerInputReader))] private UnityEngine.Object _inputReader;
        [SerializeField] private Movement2D movement;
        [SerializeField] private Weapon weapon;

        private IPlayerInputReader inputReader;
        private bool isAiming = false;

        [field: SerializeField] public string Name { get; set; } = "Player 1";
        public int Points { get; private set; } = 0;

        public Component AsComponent => this;

        public event Action<ITanxPlayer> OnPointAdded;
        public event Action<ITurnablePlayer> OnTurnFinished;

        private void Awake()
        {
            inputReader = _inputReader.As<IPlayerInputReader>();
        }

        private void OnEnable()
        {
            inputReader.OnMovementRequested += OnInputMovement;
            inputReader.OnAimRequested += OnAimRequested;
        }

        private void OnDisable()
        {
            inputReader.OnMovementRequested -= OnInputMovement;
            inputReader.OnAimRequested -= OnAimRequested;
            inputReader.OnShootRequested -= OnShootRequested;
        }

        private void OnInputMovement(float obj)
        {
            if(isAiming)
            {
                weapon.ChangeAim(-obj);
                return;
            }
            movement.Move(obj);
        }

        private void OnAimRequested()
        {
            inputReader.OnAimRequested -= OnAimRequested;
            isAiming = true;
            inputReader.OnShootRequested += OnShootRequested;
        }

        private void OnShootRequested()
        {
            inputReader.OnShootRequested -= OnShootRequested;
            weapon.OnHit += OnHit;
            weapon.Shoot();
            isAiming = false;
        }

        private void OnHit(GameObject obj)
        {
            weapon.OnHit -= OnHit;
            if(obj.GetComponent<ITanxPlayer>() != null)
            {
                Points++;
                OnPointAdded?.Invoke(this);
            }
            OnTurnFinished?.Invoke(this);
        }

        public void StartGame()
        {
            this.enabled = false;
        }

        public void StartTurn()
        {
            this.enabled = true;
        }

        public void TerminateTurn()
        {
            this.enabled = false;
        }

        private void OnDestroy()
        {
            OnDisable();
        }

        public void FinalizeTurn()
        {
            this.enabled = false;
        }
    }
}
