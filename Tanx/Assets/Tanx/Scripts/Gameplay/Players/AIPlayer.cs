﻿using QuasarBeam.Common;
using QuasarBeam.DDD;
using System;
using Tanx.Gameplay.Players.AI;
using UnityEngine;
using static UnityEngine.UI.CanvasScaler;

namespace Tanx.Gameplay.Players
{
    public class AIPlayer : MonoBehaviour, ITanxPlayer
    {
        [SerializeField] private Weapon weapon;
        [SerializeField] private Movement2D movement;
        [SerializeField, As(typeof(IReadRepository<int, ITanxPlayer>))] private UnityEngine.Object _players;
        [SerializeField] private float desiredDistanceToTarget = 10f;
        [SerializeField] private float aimingAngleTreshold = 10f;
        [SerializeField] private float aimingGiveUpTime = 1;
        [SerializeField] private Transform bulletSpawnTransform;

        [field: SerializeField] public string Name {get; set;}

        private StateMachine stateMachine;

        public int Points { get; private set; }
        public Component AsComponent => this;

        public event Action<ITanxPlayer> OnPointAdded;
        public event Action<ITurnablePlayer> OnTurnFinished;

        private bool IsEnabledGetter => this.enabled;

        private void Awake()
        {
            BuildStateMachine();

            weapon.OnHit += Weapon_OnHit;
        }

        private void BuildStateMachine()
        {
            ChosenTargetContainer targetContainer = new ChosenTargetContainer();

            IdleState idleState = new IdleState(targetContainer);
            ChooseTargetState chooseTargetState = new ChooseTargetState(targetContainer, _players.As<IReadRepository<int, ITanxPlayer>>(), this);
            MoveState moveState = new MoveState(targetContainer, movement, desiredDistanceToTarget);
            AimState aimState = new AimState(targetContainer, weapon, bulletSpawnTransform, aimingGiveUpTime, aimingAngleTreshold);
            ShotState shotState = new ShotState(weapon);

            var idleToChooseTarget = new BoolFuncDelegateTransition(idleState, chooseTargetState, () => IsEnabledGetter);
            var chooseToMove = new BoolFuncDelegateTransition(chooseTargetState, moveState, () => targetContainer.Target != null);
            var moveToAim = new BoolFuncDelegateTransition(moveState, aimState, () => moveState.Finished);
            var aimToShot = new BoolFuncDelegateTransition(aimState, shotState, () => aimState.Finished);
            var shotToIdle = new BoolFuncDelegateTransition(shotState, idleState, () => !IsEnabledGetter);

            stateMachine = new StateMachine(idleState,
                idleToChooseTarget,
                chooseToMove,
                moveToAim,
                aimToShot,
                shotToIdle
                );
        }

        private void Weapon_OnHit(GameObject obj)
        {
            if (obj.GetComponent<ITanxPlayer>() != null)
            {
                Points++;
                OnPointAdded?.Invoke(this);
            }
            OnTurnFinished?.Invoke(this);
        }

        private void Update()
        {
            stateMachine.Tick();
        }

        public void FinalizeTurn()
        {
            this.enabled = false;
            stateMachine.Tick();
        }

        public void StartGame()
        {
            this.enabled = false;
        }

        public void StartTurn()
        {
            this.enabled = true;
        }

        public void TerminateTurn()
        {
            this.enabled = false;
        }

        public class ChosenTargetContainer
        {
            public ITanxPlayer Target { get; set; }
        }
    }
}
