﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Tanx.MainMenu
{
    internal class PlayersConfigView : UIBehaviour
    {
        [SerializeField] private PlayerConfigView playerRowPrefab;
        [SerializeField] private Transform viewsParent;
        [SerializeField] private List<PlayerConfigView> currentViews;
        [SerializeField] private GameObject addPlayerButton;

        private UIElementsPoolHelper<PlayerConfigView> poolHelper;

        public event Action<int> OnPlayerColorChangeRequested;
        public event Action<int, string> OnPlayerNameChangeRequested;
        public event Action<int> OnPlayerTypeChangeRequested;
        public event Action<int> OnPlayerRemoveRequested;
        public event Action OnPlayerAddRequested;

        protected override void Awake()
        {
            poolHelper = new UIElementsPoolHelper<PlayerConfigView>(playerRowPrefab, viewsParent, currentViews);
            poolHelper.OnNewInstantiated += OnNewViewInstantiated;
            for (int i = 0; i < currentViews.Count; i++)
            {
                OnNewViewInstantiated(i, currentViews[i]);
            }
        }

        private void OnNewViewInstantiated(int index, PlayerConfigView obj)
        {
            obj.OnColorChangeRequested += () => OnColorChangeRequested(index);
            obj.OnTypeChangeRequested += () => OnTypeChangeRequested(index);
            obj.OnNameChangeRequested += (s) => OnNameChangeRequested(index, s);
            obj.OnRemoveRequested += () => OnRemoveRequested(index);
        }

        private void OnRemoveRequested(int index)
        {
            OnPlayerRemoveRequested(index);
        }

        private void OnNameChangeRequested(int index, string s)
        {
            OnPlayerNameChangeRequested?.Invoke(index, s);
        }

        private void OnTypeChangeRequested(int index)
        {
            OnPlayerTypeChangeRequested?.Invoke(index);
        }

        private void OnColorChangeRequested(int index)
        {
            OnPlayerColorChangeRequested?.Invoke(index);
        }

        public void RequestAddPlayer()
        {
            OnPlayerAddRequested?.Invoke();
        }

        public void Show(Data data)
        {
            poolHelper.Show(data.players.Count, (i, v) => v.Show(data.players[i]));
            addPlayerButton.SetActive(data.canAddPlayer);
            if (data.canAddPlayer)
            {
                addPlayerButton.transform.SetAsLastSibling();
            }
        }

        public struct Data
        {
            public readonly IList<Player> players;
            public readonly bool canAddPlayer;

            public Data(IList<Player> players, bool canAddPlayer)
            {
                this.players = players ?? throw new ArgumentNullException(nameof(players));
                this.canAddPlayer = canAddPlayer;
            }

            public struct Player
            {
                public readonly string name;
                public readonly Color color;
                public readonly bool isAI;
                public readonly bool canBeRemoved;

                public Player(string name, Color color, bool isAI, bool canBeRemoved)
                {
                    this.name = name ?? throw new ArgumentNullException(nameof(name));
                    this.color = color;
                    this.isAI = isAI;
                    this.canBeRemoved = canBeRemoved;
                }
            }
        }
    }
}