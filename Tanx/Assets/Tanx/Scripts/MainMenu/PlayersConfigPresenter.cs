﻿using System.Collections.Generic;
using System.Linq;
using Tanx.Gameplay;
using UnityEngine;

namespace Tanx.MainMenu
{
    internal class PlayersConfigPresenter : MonoBehaviour
    {
        [SerializeField] private PlayersConfig config;
        [SerializeField] private PlayersConfigView configView;
        [SerializeField] private List<Color> availablePlayerColors;

        private void Start()
        {
            configView.OnPlayerColorChangeRequested += OnPlayerColorChangeRequested;
            configView.OnPlayerNameChangeRequested += OnPlayerNameChangeRequested;
            configView.OnPlayerTypeChangeRequested += OnPlayerTypeChangeRequested;
            configView.OnPlayerRemoveRequested += OnPlayerRemoveRequested;
            configView.OnPlayerAddRequested += OnPlayerAddRequested;
            UpdateView();
        }

        private void OnPlayerAddRequested()
        {
            var add = new PlayerData($"{config.PlayerAddPrototype.Name} {config.Players.Count}", config.PlayerAddPrototype.type, config.PlayerAddPrototype.color);
            config.Players.Add(add);
            UpdateView();
        }

        private void OnPlayerRemoveRequested(int obj)
        {
            config.Players.RemoveAt(obj);
            UpdateView();
        }

        private void OnPlayerTypeChangeRequested(int obj)
        {
            config.Players[obj].type = config.Players[obj].type == PlayerData.Type.HUMAN ? PlayerData.Type.AI : PlayerData.Type.HUMAN;
            UpdateView();
        }

        private void OnPlayerNameChangeRequested(int arg1, string arg2)
        {
            if (string.IsNullOrWhiteSpace(arg2))
            {
                UpdateView();
                return;
            }
            config.Players[arg1].Name = arg2;
            UpdateView();
        }

        private void OnPlayerColorChangeRequested(int obj)
        {
            int colorIndex = availablePlayerColors.IndexOf(config.Players[obj].color);
            colorIndex++;//if color is not found, the index was -1 and is now 0
            if(colorIndex >= availablePlayerColors.Count)
            {
                colorIndex = 0;
            }
            config.Players[obj].color = availablePlayerColors[colorIndex];
            UpdateView();
        }

        private void UpdateView()
        {
            bool canRemoveAnyPlayer = config.Players.Count > config.MinimumPlayerCount;
            bool canAddPlayer = config.Players.Count < config.MaximumPlayerCount;
            var playersViewData = config.Players.Select(p => new PlayersConfigView.Data.Player(p.Name, p.color, p.type == PlayerData.Type.AI, canRemoveAnyPlayer)).ToArray();
            configView.Show(new PlayersConfigView.Data(playersViewData, canAddPlayer));
        }
    }
}
