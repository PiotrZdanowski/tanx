﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Tanx.MainMenu
{
    internal class PlayerConfigView : UIBehaviour
    {
        [SerializeField] private Image colorImage;
        [SerializeField] private TMP_InputField playerNameField;
        [SerializeField] private GameObject isAIIndicatorOn;
        [SerializeField] private GameObject isPlayerIndicatorOn;
        [SerializeField] private GameObject removeButton;

        public event Action OnColorChangeRequested;
        public event Action OnTypeChangeRequested;
        public event Action OnRemoveRequested;
        public event Action<string> OnNameChangeRequested;

        internal void Show(PlayersConfigView.Data.Player player)
        {
            colorImage.color = player.color;
            playerNameField.text = player.name;
            isAIIndicatorOn.SetActive(player.isAI);
            isPlayerIndicatorOn.SetActive(!player.isAI);
            removeButton.SetActive(player.canBeRemoved);
        }

        public void RequestColorChange()
        {
            OnColorChangeRequested?.Invoke();
        }

        public void RequestNameChange()
        {
            OnNameChangeRequested?.Invoke(playerNameField.text);
        }

        public void RequestTypeChange()
        {
            OnTypeChangeRequested?.Invoke();
        }

        public void RequestRemove()
        {
            OnRemoveRequested?.Invoke();
        }
    }
}