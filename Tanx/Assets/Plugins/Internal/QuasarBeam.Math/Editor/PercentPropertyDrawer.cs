﻿using UnityEditor;
using UnityEngine;

namespace QuasarBeam.Math
{
    [CustomPropertyDrawer(typeof(Percent))]
    internal class PercentPropertyDrawer : PropertyDrawer
    {
        private const int PERCENT_LABEL_WIDTH = 20;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var percentRect = new Rect(position.x + position.width - PERCENT_LABEL_WIDTH, position.y, PERCENT_LABEL_WIDTH, position.height);
            var valueRect = new Rect(position.x, position.y, position.width - PERCENT_LABEL_WIDTH, position.height);

            TextAnchor alignment = EditorStyles.textField.alignment;
            EditorStyles.textField.alignment = TextAnchor.MiddleRight;
            EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("value"), GUIContent.none);
            EditorStyles.textField.alignment = alignment;
            EditorGUI.LabelField(percentRect, "%");

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}