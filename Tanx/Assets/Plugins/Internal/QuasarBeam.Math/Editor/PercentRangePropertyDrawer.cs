﻿using UnityEditor;
using UnityEngine;

namespace QuasarBeam.Math
{
    [CustomPropertyDrawer(typeof(PercentRange))]
    internal class PercentRangePropertyDrawer : PropertyDrawer
    {
        private const int ARROW_LABEL_WIDTH = 20;
        private const string ARROW_LABEL = "↔";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var minRect = new Rect(position.x, position.y, (position.width - ARROW_LABEL_WIDTH) / 2, position.height);
            var labelRect = new Rect(position.x + minRect.width, position.y, ARROW_LABEL_WIDTH, position.height);
            var maxRect = new Rect(position.x + minRect.width + labelRect.width, position.y, (position.width - ARROW_LABEL_WIDTH) / 2, position.height);

            EditorGUI.PropertyField(minRect, property.FindPropertyRelative("min"), GUIContent.none);

            TextAnchor originalAlignment = EditorStyles.textField.alignment;
            EditorStyles.textField.alignment = TextAnchor.MiddleCenter;
            EditorGUI.LabelField(labelRect, ARROW_LABEL);
            EditorStyles.textField.alignment = originalAlignment;

            EditorGUI.PropertyField(maxRect, property.FindPropertyRelative("max"), GUIContent.none);

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}