﻿namespace QuasarBeam.Math
{
    public static class PercentExtensions
    {

        /// <summary>
        /// Creates a Percent from a Single. a value of 100 returns 100%
        /// </summary>
        /// <param name="value">A value of 100 returns 100%</param>
        public static Percent Percent(this float value)
        {
            return Math.Percent.FromFraction(value, 100);
        }

        /// <summary>
        /// Creates a Percent from an integer. a value of 100 returns 100%
        /// </summary>
        /// <param name="value">A value of 100 returns 100%</param>
        public static Percent Percent(this int value)
        {
            return Percent((float)value);
        }

        /// <summary>
        /// Checks if the Percent is in range
        /// </summary>
        /// <returns>true if percent is withing range, otherwise false</returns>
        public static bool IsIn(this Percent percent, PercentRange range, bool minInclusive = true, bool maxInclusive = false)
        {
            if(!minInclusive && percent == range.min)
            {
                return false;
            }
            if(!maxInclusive && percent == range.max)
            {
                return false;
            }
            return range.min < percent && range.max > percent;
        }
    }
}
