﻿using System;

namespace QuasarBeam.Math
{
    [Serializable]
    public struct PercentRange
    {
        public Percent min;
        public Percent max;

        public PercentRange(Percent min, Percent max)
        {
            this.min = min;
            this.max = max;
        }

        public Percent Evaluate(Percent percent)
        {
            return ((max - min) * percent) + min;
        }
    }
}
