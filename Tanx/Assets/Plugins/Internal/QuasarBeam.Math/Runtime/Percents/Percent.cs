﻿using System;
using UnityEngine;

namespace QuasarBeam.Math
{
    [Serializable]
    public struct Percent : IEquatable<Percent>, IEquatable<float>
    {
        [SerializeField]
        private float value;

        private Percent(float value)
        {
            this.value = value;
        }

        public override bool Equals(object obj)
        {
            bool isEqualToOtherPercent = (obj is Percent percent && Equals(percent));
            bool isEqualToOtherFloat = (obj is float f && Equals(f));
            return isEqualToOtherPercent || isEqualToOtherFloat;
        }

        public bool Equals(Percent other)
        {
            return other.GetHashCode() == GetHashCode();
        }

        public bool Equals(float f)
        {
            return Mathf.Approximately(f, this);
        }

        public override int GetHashCode()
        {
            return -1584136870 + value.GetHashCode();
        }

        public override string ToString()
        {
            return $"{value}%";
        }

        public Percent Inverted()
        {
            return Invert(this);
        }

        public Percent Clamped()
        {
            float newValue = value;
            if(newValue > 100)
            {
                newValue = 100;
            }
            if(newValue < 0)
            {
                newValue = 0;
            }
            return new Percent(newValue);
        }

        public static implicit operator float(Percent p)
        {
            return p.value / 100f;
        }

        #region creation
        public static Percent FromFraction(float numerator, float denominator)
        {
            return new Percent(numerator * 100 / denominator);
        }

        public static Percent From01Value(float value01)
        {
            return new Percent(value01 * 100);
        }

        public static Percent Invert(Percent percent)
        {
            return new Percent(-(percent.value - 100));
        }
        #endregion

        #region math
        public static Percent operator *(Percent one, Percent two)
        {
            return new Percent((float)one * two * 100f);
        }

        public static Percent operator *(Percent one, float f)
        {
            return new Percent(((float)one * f) * 100f);
        }

        public static Percent operator +(Percent one, Percent two)
        {
            return new Percent(((float)one + two) * 100f);
        }

        public static Percent operator -(Percent one, Percent two)
        {
            return new Percent(((float)one - two) * 100f);
        }

        public static Percent operator -(Percent one)
        {
            return new Percent((-(float)one) * 100f);
        }
        #endregion
    }
}
