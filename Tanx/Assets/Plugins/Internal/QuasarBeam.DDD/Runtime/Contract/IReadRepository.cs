using System.Collections.Generic;

namespace QuasarBeam.DDD
{
    public interface IReadRepository<TID, TEntity> : IEnumerable<RepositoryEntry<TID, TEntity>>
    {
        RepositoryEntry<TID, TEntity> Get(TID id); 
        TID GetID(TEntity entity);

        bool ContainsID(TID id);
        bool Contains(TEntity entity);
    }
}
