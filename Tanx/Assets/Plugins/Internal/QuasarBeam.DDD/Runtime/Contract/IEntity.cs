namespace QuasarBeam.DDD
{
    public interface IEntity<TID>
    {
        TID ID { get; }
    }
}
