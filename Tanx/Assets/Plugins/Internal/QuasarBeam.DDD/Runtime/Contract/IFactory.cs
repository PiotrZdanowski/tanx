namespace QuasarBeam.DDD
{
    public interface IFactory<T>
    {
        public T Create();
    }

    public interface IFactory<TIN, TOUT>
    {
        public TOUT Create(TIN arg);
    }
}
