using System;
using System.Collections.Generic;

namespace QuasarBeam.DDD
{
    public readonly struct RepositoryEntry<TID, TEntity> : IEquatable<RepositoryEntry<TID, TEntity>>
    {
        public readonly TID ID;
        public readonly TEntity Entity;

        public RepositoryEntry(TID iD, TEntity entity)
        {
            ID = iD;
            Entity = entity;
        }

        public static implicit operator TID(RepositoryEntry<TID, TEntity> entry)
        {
            return entry.ID;
        }

        public static implicit operator TEntity(RepositoryEntry<TID, TEntity> entry)
        {
            return entry.Entity;
        }

        public static implicit operator KeyValuePair<TID, TEntity>(RepositoryEntry<TID, TEntity> entry)
        {
            return KeyValuePair.Create(entry.ID, entry.Entity);
        }

        public bool Equals(RepositoryEntry<TID, TEntity> other)
        {
            if(this.ID is null && this.Entity is null)
            {
                return other.ID is null && other.Entity is null;
            }
            if (this.ID is null && this.Entity is not null)
            {
                return other.ID is null && this.Entity.Equals(other.Entity);
            }
            return this.ID.Equals(other.ID) && this.Entity.Equals(other.Entity);
        }

        public override bool Equals(object obj)
        {
            if(obj is  RepositoryEntry<TID, TEntity> otherEntry)
            {
                return Equals(otherEntry);
            }
            return false;
        }

        public override string ToString()
        {
            return $"{ID}: {Entity}";
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ID, Entity);
        }
    }
}
