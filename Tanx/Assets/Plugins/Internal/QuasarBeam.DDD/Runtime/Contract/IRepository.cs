namespace QuasarBeam.DDD
{
    public interface IRepository<TID, TEntity> : IReadRepository<TID, TEntity>
    {
        void Insert(TID id, TEntity entity);
        bool Remove(TID id);
    }
}
