namespace QuasarBeam.DDD
{
    public static class RepositoryExtensions
    {
        public static void Insert<TID, TEntity>(this IRepository<TID, TEntity> repository, TEntity entity) where TEntity : IEntity<TID>
        {
            repository.Insert(entity.ID, entity);
        }
    }
}
