using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QuasarBeam.DDD
{
    public class RuntimeGenericRegistry<TID, TEntity> : IRepository<TID, TEntity>
    {
        private Dictionary<TID, TEntity> internalDictionary = new Dictionary<TID, TEntity>();

        public bool ContainsID(TID id)
        {
            return internalDictionary.ContainsKey(id);
        }

        public bool Contains(TEntity entity)
        {
            return internalDictionary.ContainsValue(entity);
        }

        public RepositoryEntry<TID, TEntity> Get(TID id)
        {
            if (!ContainsID(id))
            {
                throw new InvalidOperationException($"There is no object with {id} in {this}!");
            }
            return new RepositoryEntry<TID, TEntity>(id, internalDictionary[id]);
        }

        public IEnumerator<RepositoryEntry<TID, TEntity>> GetEnumerator()
        {
            foreach(var kvp in internalDictionary)
            {
                yield return new RepositoryEntry<TID, TEntity>(kvp.Key, kvp.Value);
            }
        }

        public TID GetID(TEntity entity)
        {
            if(entity is IEntity<TID> ent)
            {
                return ent.ID;
            }
            var find = internalDictionary.FirstOrDefault(kvp => kvp.Value.Equals(entity));
            if (find.Equals(default(KeyValuePair<TID, TEntity>)))
            {
                throw new InvalidOperationException($"Object {entity} does not exist in {this}!");
            }
            return find.Key;
        }

        public void Insert(TID id, TEntity entity)
        {
            if(!internalDictionary.TryAdd(id, entity))
            {
                throw new InvalidOperationException($"ID {id} was already added into {this}!");
            }
        }

        public bool Remove(TID id)
        {
            if (!ContainsID(id))
            {
                return false;
            }
            return internalDictionary.Remove(id);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
