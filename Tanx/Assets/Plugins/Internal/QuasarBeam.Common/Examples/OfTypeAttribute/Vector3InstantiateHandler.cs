using UnityEngine;

namespace QuasarBeam.Common.Examples.OfType
{
    public class Vector3InstantiateHandler : MonoBehaviour, IExampleInterface<Vector3>
    {
        [SerializeField] private GameObject objectToInstantiate;

        public void DoAThing(Vector3 t)
        {
            Instantiate(objectToInstantiate, t, Quaternion.identity, transform);
        }
    }
}
