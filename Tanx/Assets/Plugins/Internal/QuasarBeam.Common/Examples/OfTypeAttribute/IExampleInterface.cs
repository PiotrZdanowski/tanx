namespace QuasarBeam.Common.Examples.OfType
{
    internal interface IExampleInterface<T>
    {
        void DoAThing(T t);
    }
}
