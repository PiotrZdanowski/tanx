using UnityEngine;

namespace QuasarBeam.Common.Examples.OfType
{
    public class Vector3TransformPositioningHandler : MonoBehaviour, IExampleInterface<Vector3>
    {
        public void DoAThing(Vector3 t)
        {
            transform.position = t;
        }
    }
}
