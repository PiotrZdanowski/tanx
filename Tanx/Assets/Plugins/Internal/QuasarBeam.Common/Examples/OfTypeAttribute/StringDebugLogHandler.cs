using UnityEngine;

namespace QuasarBeam.Common.Examples.OfType
{
    public class StringDebugLogHandler : MonoBehaviour, IExampleInterface<string>
    {
        public void DoAThing(string t)
        {
            Debug.Log(t);
        }
    }
}
