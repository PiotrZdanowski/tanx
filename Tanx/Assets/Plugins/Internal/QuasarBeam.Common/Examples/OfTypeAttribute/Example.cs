using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace QuasarBeam.Common.Examples.OfType
{
    internal class Example : MonoBehaviour
    {
        [SerializeField] private Vector3 exampleVector3Value;
        [field: SerializeField, As(typeof(IExampleInterface<Vector3>))] public UnityEngine.Object Vector3Handler { get; private set; }
        [SerializeField] private string exampleStringValue;
        [field: SerializeField, As(typeof(IExampleInterface<string>))] public UnityEngine.Object StringHandler { get; private set; }
        [field: SerializeField, As(typeof(IExampleInterface<Vector3>))] public UnityEngine.Object[] Vector3HandlersArray { get; private set; }
        [field: SerializeField, As(typeof(IExampleInterface<string>))] public List<UnityEngine.Object> StringHandlersList { get; private set; }

        private void Start()
        {
            Run();
        }

        [ContextMenu("Run Example")]
        public void Run()
        {
            Vector3Handler.As<IExampleInterface<Vector3>>().DoAThing(exampleVector3Value);
            StringHandler.As<IExampleInterface<string>>().DoAThing(exampleStringValue);
            int i = 0;
            foreach(var handler in Vector3HandlersArray.OfType<IExampleInterface<Vector3>>())
            {
                handler.DoAThing(exampleVector3Value + Vector3.right * i);
                i++;
            }
            i = 0;
            foreach (var handler in StringHandlersList.OfType<IExampleInterface<string>>())
            {
                handler.DoAThing($"{exampleStringValue} {i}");
                i++;
            }
        }
    }
}
