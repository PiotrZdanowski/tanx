using UnityEngine;

namespace QuasarBeam.Common.Integration
{
    [CreateAssetMenu(menuName = CreateMenuEndpoints.CREATE_ASSET + "Random")]
    public sealed class RandomSO : ScriptableObject, IRandom, IInit, IInit<int>
    {
        [SerializeField] private int seed;
        [SerializeField] private bool initializeSeedOnAwake = true;

        private System.Random internalRandom;

        private void Awake()
        {
            if (initializeSeedOnAwake)
            {
                Init();
            }
            else
            {
                Init(seed);
            }
        }

        [ContextMenu("Init with random seed")]
        public void Init()
        {
            System.Random rnd = new System.Random();
            int newSeed = rnd.Next();
            Init(seed);
        }

        public void Init(int seed)
        {
            this.seed = seed;
            internalRandom = new System.Random(seed);
        }

        public int Next(int minValue, int maxValue)
        {
            return internalRandom.Next(minValue, maxValue);
        }

        public float NextValue()
        {
            return (float)internalRandom.NextDouble();
        }
    }
}
