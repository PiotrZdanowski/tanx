using UnityEditor;
using UnityEngine;

namespace QuasarBeam.Common.Editor
{

    [CustomPropertyDrawer(typeof(AsAttribute))]

    public class AsAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.ObjectReference)
            {
                var attribute = this.attribute as AsAttribute;
                EditorGUI.BeginProperty(position, label, property);
                var previousColor = GUI.color;
                if (property.objectReferenceValue == null)
                {
                    GUI.color = Color.red;
                }
                label.text = $"{label.text} ({attribute.RequestedType.CSharpName()})";
                Object source = EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(Object), true);
                GUI.color = previousColor;
                if (source.TryRetrieve(attribute.RequestedType, out UnityEngine.Object retrieved))
                {
                    property.objectReferenceValue = retrieved;
                }
                else
                {
                    property.objectReferenceValue = null;
                }
                EditorGUI.EndProperty();
            }
            //TODO: SerializedPropertyType.ManagedReference
            else
            {
                var previousColor = GUI.color;
                GUI.color = Color.red;
                EditorGUI.LabelField(position, label, new GUIContent($"Property type {property.propertyType} is not supported!"));
                GUI.color = previousColor;
            }
        }

    }
}
