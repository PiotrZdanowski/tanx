using System;
using UnityEngine;

namespace QuasarBeam.Common
{
    public class AsAttribute : PropertyAttribute
    {
        public Type RequestedType { get; private set; }

        public AsAttribute(Type requestedType)
        {
            RequestedType = requestedType ?? throw new ArgumentNullException(nameof(requestedType));
        }
    }
}
