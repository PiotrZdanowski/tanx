﻿using System;
using System.Collections.Generic;

namespace QuasarBeam.Common
{
    public struct ValidationResult
    {
        public readonly bool Success;
        public readonly IEnumerable<string> FailureMessages;

        public bool Failure => !Success;

        private ValidationResult(bool success, IEnumerable<string> failureMessages)
        {
            Success = success;
            FailureMessages = failureMessages ?? throw new ArgumentNullException(nameof(failureMessages));
        }

        public static ValidationResult Successful()
        {
            return new ValidationResult(true, Array.Empty<string>());
        }

        public static ValidationResult Failed(IEnumerable<string> failureMessages)
        {
            return new ValidationResult(false, failureMessages);
        }

        public static ValidationResult Failed(params string[] failureMessages)
        {
            return new ValidationResult(false, failureMessages);
        }
    }
}