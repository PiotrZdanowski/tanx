﻿namespace QuasarBeam.Common
{
    public interface IValidator<T>
    {
        ValidationResult Validate(T value);
    }
}