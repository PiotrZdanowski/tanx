﻿using System.Collections.Generic;

namespace QuasarBeam.Common
{
    public interface IValidationResultProvider : IChangeable<IValidationResultProvider>
    {
        ValidationResult LastValidationResult { get; }
    }
}