﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuasarBeam.Common
{
    public class QuickValidator<T> : IValidator<T>
    {
        private ICollection<Rule> rules = new LinkedList<Rule>();

        public ValidationResult Validate(T value)
        {
            LinkedList<string> validationFailureMessages = new LinkedList<string>();
            bool failedAtLeastOnce = false;

            try
            {
                foreach (var rule in rules)
                {
                    bool failed = !rule.rule.Invoke(value);
                    if (failed)
                    {
                        validationFailureMessages.AddLast(rule.failureMessage.Invoke(value));
                        failedAtLeastOnce = true;
                    }
                }   
            }
            catch (Exception e)
            {
                validationFailureMessages.AddLast($"Couldn't fully validate due to an error in rules: \n{e}");
                failedAtLeastOnce = true;
            }
            if (failedAtLeastOnce)
            {
                return ValidationResult.Failed(validationFailureMessages);
            }
            return ValidationResult.Successful();
        }

        public QuickValidator<T> AddRule(Predicate<T> rule, Func<T, string> failureMessage)
        {
            rules.Add(new Rule(rule, failureMessage));
            return this;
        }

        public void RemoveRule(Predicate<T> rule)
        {
            var toRemove = rules.Where(r => r.rule == rule).ToArray();
            foreach(var ruleToRemove in toRemove)
            {
                rules.Remove(ruleToRemove);
            }
        }

        private readonly struct Rule
        {
            public readonly Predicate<T> rule;
            public readonly Func<T, string> failureMessage;

            public Rule(Predicate<T> rule, Func<T, string> failureMessage)
            {
                this.rule = rule ?? throw new ArgumentNullException(nameof(rule));
                this.failureMessage = failureMessage ?? throw new ArgumentNullException(nameof(failureMessage));
            }
        }

        public T ValidateAndThrowOnFail(T value, string exceptionMessage = null)
        {
            var result = Validate(value);
            if (result.Failure)
            {
                StringBuilder sb = new StringBuilder();
                if(exceptionMessage == null)
                {
                    exceptionMessage = $"{value} validation failed. Check details.";
                }
                sb.Append(exceptionMessage);
                foreach(var message in result.FailureMessages)
                {
                    sb.AppendLine(message);
                }
                throw new InvalidOperationException(sb.ToString());
            }
            return value;
        }
    }
}