﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace QuasarBeam.Common
{
    [Serializable]
    public class IndexGenerator : ScriptableObject
    {
        private int currentReturned = -1;
        private Queue<int> freeIndexes = new Queue<int>();

        public int GetNext()
        {
            if(freeIndexes.Count > 0)
            {
                return freeIndexes.Dequeue();
            }
            currentReturned++;
            return currentReturned;
        }

        public void Free(int index)
        {
            if(index > currentReturned)
            {
                return;
            }
            if(index == currentReturned)
            {
                currentReturned--;
                return;
            }
            freeIndexes.Enqueue(index);
        }

        public void EnforceTaken(int index)
        {
            if(index < currentReturned + 1)
            {
                freeIndexes.Remove(index);
                return;
            }
            int maxi = index - currentReturned;
            for (int i = 1; i < maxi; i++)
            {
                freeIndexes.Enqueue(currentReturned + i);
            }
            currentReturned = index;
        }
    }
}