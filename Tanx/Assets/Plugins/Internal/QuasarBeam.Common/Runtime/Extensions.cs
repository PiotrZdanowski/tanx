using System;
using System.Linq;
using System.Text;
using UnityEngine;

namespace QuasarBeam.Common
{
    public static class Extensions
    {
        public static T As<T>(this object o) where T : class
        {
            return o as T;
        }

        /// <summary>
        /// Try to retrieve an object of given type from a UnityEngine.Object
        /// </summary>
        /// <param name="source">Source of the oject of given type.</param>
        /// <param name="t">Type of object that is to be retrieved</param>
        /// <param name="retrieved">Result</param>
        /// <returns>true if the object of given type is found on a source, otherwise false</returns>
        public static bool TryRetrieve(this UnityEngine.Object source, Type t, out UnityEngine.Object retrieved)
        {
            if (source == null || source is null)
            {
                retrieved = null;
                return false;
            }
            if (t.IsAssignableFrom(source.GetType()))
            {
                retrieved = source;
                return true;
            }
            if(source is GameObject go && go.TryGetComponent(t, out Component compFromGO))
            {
                retrieved = compFromGO;
                return true;
            }
            if (source is Component comp && comp.TryGetComponent(t, out Component compFromComp))
            {
                retrieved = compFromComp;
                return true;
            }
            retrieved = null;
            return false;
        }

        /// <summary>
        /// Get a c# format name of a type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string CSharpName(this Type type)
        {
            var sb = new StringBuilder();
            var name = type.Name;
            if (!type.IsGenericType) return name;
            sb.Append(name.Substring(0, name.IndexOf('`')));
            sb.Append("<");
            sb.Append(string.Join(", ", type.GetGenericArguments()
                                            .Select(t => t.CSharpName())));
            sb.Append(">");
            return sb.ToString();
        }
    }
}
