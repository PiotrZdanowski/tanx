using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace QuasarBeam.Common
{
    public static class RandomExtensions
    {
        public static T RandomElementFrom<T>(this IRandom randomizer, IList<T> list)
        {
            if(list.Count == 0)
            {
                throw new System.ArgumentOutOfRangeException($"The {list} has no elements to randomize!");
            }
            int random = randomizer.Next(0, list.Count);
            return list[random];
        }

        public static T RandomElementFrom<T>(this IRandom randomizer, ICollection<T> collection)
        {
            if(collection is IList<T> list)
            {
                return randomizer.RandomElementFrom(list);
            }
            if (collection.Count == 0)
            {
                throw new System.ArgumentOutOfRangeException($"The {collection} has no elements to randomize!");
            }
            int random = randomizer.Next(0, collection.Count);
            return collection.ElementAt(random);
        }

        public static T RandomElementFrom<T>(this IRandom randomizer, IEnumerable<T> enumerable)
        {
            if (enumerable is ICollection<T> collection)
            {
                return randomizer.RandomElementFrom(collection);
            }
            int count = enumerable.Count();
            if (count == 0)
            {
                throw new System.ArgumentOutOfRangeException($"The {enumerable} has no elements to randomize!");
            }
            int random = randomizer.Next(0, count);
            return enumerable.ElementAt(random);
        }
    }
}
