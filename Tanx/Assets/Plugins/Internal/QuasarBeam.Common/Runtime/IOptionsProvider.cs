﻿namespace QuasarBeam.Common
{
    public interface IOptionsProvider<T> : IChangeable<IOptionsProvider<T>, T>
    {
        T Options { get; }
    }
}