﻿using UnityEditor;
using UnityEngine;

namespace QuasarBeam.Common
{
    public interface IChangeable
    {
        public delegate void ChangeHandler(IChangeable sender);

        public event ChangeHandler OnChanged;
    }

    public interface IChangeable<TSender>
    {
        public delegate void ChangeHandler(IChangeable<TSender> sender);

        public event ChangeHandler OnChanged;
    }

    public interface IChangeable<TSender, TData>
    {
        public delegate void ChangeHandler(IChangeable<TSender, TData> sender, TData newData);

        public event ChangeHandler OnChanged;
    }
}