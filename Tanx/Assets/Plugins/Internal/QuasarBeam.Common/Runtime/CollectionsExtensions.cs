using System.Collections.Generic;
using System.Linq;

namespace QuasarBeam.Common
{
    public static class CollectionsExtensions
    {
        public static bool TryFirst<T>(this IEnumerable<T> enumerable, out T firstOrDefault)
        {
            firstOrDefault = enumerable.FirstOrDefault();
            return !(firstOrDefault is null) && !firstOrDefault.Equals(default);
        }

        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key)
        {
            return dict.GetOrDefault(key, default);
        }

        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue @default)
        {
            if(dict.TryGetValue(key, out var value))
            {
                return value;
            }
            return @default;
        }

        public static T[] CombinedWith<T>(this T[] array, params T[][] otherArrays)
        {
            var count = array.Length + otherArrays.Aggregate<T[], int>(0, (sum, a) => sum + a.Length);
            var result = new T[count];
            int offset = 0;
            array.CopyTo(result, offset);
            offset += array.Length;
            for(int i = 0; i < otherArrays.Length; i++)
            {
                otherArrays[i].CopyTo(result, offset);
                offset += otherArrays[i].Length;
            }
            return result;
        }

        public static bool Remove<T>(this Queue<T> queue, T element)
        {
            int counter = queue.Count;
            T current;
            bool removed = false;
            for (; counter --> 0;)
            {
                current = queue.Dequeue();
                if(current is null)
                {
                    if(element is null)
                    {
                        removed = true;
                        continue;
                    }
                }
                else if (current.Equals(element))
                {
                    removed = true;
                    continue;
                }
                queue.Enqueue(current);
            }
            return removed;
        }
    }
}
