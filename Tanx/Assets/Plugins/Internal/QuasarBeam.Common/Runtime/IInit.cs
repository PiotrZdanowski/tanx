using UnityEngine;

namespace QuasarBeam.Common
{
    public interface IInit
    {
        void Init();
    }

    public interface IInit<ARG>
    {
        void Init(ARG arg);
    }

    public interface IInit<ARG1, ARG2>
    {
        void Init(ARG1 arg1, ARG2 arg2);
    }

    public interface IInit<ARG1, ARG2, ARG3>
    {
        void Init(ARG1 arg1, ARG2 arg2, ARG3 arg3);
    }
}
