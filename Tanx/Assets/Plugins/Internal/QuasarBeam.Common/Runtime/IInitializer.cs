using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuasarBeam.Common
{
    /// <summary>
    /// An object that can initialize TARGET type
    /// </summary>
    /// <typeparam name="TARGET"></typeparam>
    public interface IInitializer<TARGET>
    {
        void Initialize(TARGET target);
    }

    /// <summary>
    /// An object that may initialize TARGET type, prividing a result of initialization
    /// </summary>
    /// <typeparam name="TARGET"></typeparam>
    public interface IInitializer<RESULT, TARGET>
    {
        RESULT TryInitialize(TARGET target);
    }
}
