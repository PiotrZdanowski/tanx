namespace QuasarBeam.Common
{
    public interface IRandom
    {
        /// <summary>
        /// Get random integer value
        /// </summary>
        /// <param name="minValue">minimum value inclusive</param>
        /// <param name="maxValue">maximum value exclusive</param>
        /// <returns>random integer value</returns>
        public int Next(int minValue, int maxValue);

        /// <summary>
        /// Get random float value
        /// </summary>
        /// <returns>random float value between 0 and 1</returns>
        public float NextValue();
    }
}
