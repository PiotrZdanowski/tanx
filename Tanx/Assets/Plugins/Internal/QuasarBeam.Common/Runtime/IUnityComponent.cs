﻿using JetBrains.Annotations;
using UnityEngine;

namespace QuasarBeam.Common
{
    public interface IUnityComponent
    {
        Component AsComponent { get; }
    }
}
